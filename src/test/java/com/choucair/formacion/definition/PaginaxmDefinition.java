package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.GeneraContraseñaSteps;
import com.choucair.formacion.steps.PaginaxmSteps;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class PaginaxmDefinition 
{	
	@Steps
	PaginaxmSteps paginaxmSteps;
	@Steps
	GeneraContraseñaSteps generaContraseñaSteps;
	
	@Given("^Ingresar a la pagina y aceptar Cookies$")
	public void ingresar_a_la_pagina_y_aceptar_Cookies()
	{
	    // Write code here that turns the phrase above into concrete actions
		paginaxmSteps.abreAcepta();
	}	
	
	@When("^Ingresar Datos Personales$")
	public void ingresar_Datos_Personales(DataTable dtDatosForm)  
	{
		List<List<String>> data = dtDatosForm.raw();
		
		for (int i=1; i<data.size(); i++) {
			paginaxmSteps.diligenciar_datos_tabla(data, i);
			try {
				Thread.sleep(10);
			}catch(InterruptedException e) {}
		}

	}
	
	@When("^Agregar datos de cuenta de trading$")
	public void agregar_datos_de_cuenta_de_trading(DataTable dtDatosForm)
	{
		List<List<String>> data = dtDatosForm.raw();
		
		for (int i=1; i<data.size(); i++) {
			paginaxmSteps.diligenciar_datos_cuenta(data, i);
			try {
				Thread.sleep(10);
			}catch(InterruptedException e) {}
		}
	}
	
	@When("^Crear Contraseña$")
	public void crear_Contraseña(DataTable dtDatosForm)
	{
			List<List<String>> data = dtDatosForm.raw();
		
		for (int i=1; i<data.size(); i++) {
			paginaxmSteps.diligenciar_datos_contraseña(data, i);
			try {
				Thread.sleep(10);
			}catch(InterruptedException e) {}
		}
	}
	
	@Then("^Abrir Cuenta demo$")
	public void abrir_Cuenta_demo(){
		paginaxmSteps.guardarDatos();
	}
	
	@Then("^Ingresar contraseña$")
	public void ingresar_contraseña()
	{
	    // Write code here that turns the phrase above into concrete actions
		generaContraseñaSteps.Contraseña();
		generaContraseñaSteps.ConfirmaContraseña();
		try {
			Thread.sleep(10);
			}catch(InterruptedException e) {}
		
	}

}
