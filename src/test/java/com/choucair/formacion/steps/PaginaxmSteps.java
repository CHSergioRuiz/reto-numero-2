package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.PaginaxmPageObject;

import net.thucydides.core.annotations.Step;


public class PaginaxmSteps 
{

	PaginaxmPageObject paginaxmPageObject;
	
	@Step
	public void abreAcepta() 
	{
		paginaxmPageObject.open();
		paginaxmPageObject.btnCookie();
	}
	
	@Step
	public void diligenciar_datos_tabla(List<List<String>> data, int id) 
	{
		paginaxmPageObject.PrimerNombre(data.get(id).get(0).trim());
		paginaxmPageObject.Apellido(data.get(id).get(1).trim());
		paginaxmPageObject.PaisDeResidencia(data.get(id).get(2).trim());
		paginaxmPageObject.Ciudad(data.get(id).get(3).trim());
		paginaxmPageObject.Telefono(data.get(id).get(4).trim());
		paginaxmPageObject.Email(data.get(id).get(5).trim());
		paginaxmPageObject.IdiomaPreferido(data.get(id).get(6).trim());

	}

	@Step
	public void diligenciar_datos_contraseña(List<List<String>> data, int id) {
		// TODO Auto-generated method stub
		paginaxmPageObject.Contraseña(data.get(id).get(0).trim());
		paginaxmPageObject.ConfirmaContraseña(data.get(id).get(1).trim());	
	}

	@Step
	public void diligenciar_datos_cuenta(List<List<String>> data, int id) {
		// TODO Auto-generated method stub
		paginaxmPageObject.TipoDePlataformaTrading(data.get(id).get(0).trim());
		paginaxmPageObject.TipoCuenta(data.get(id).get(1).trim());
		paginaxmPageObject.MonedaBase(data.get(id).get(2).trim());
		paginaxmPageObject.Apalancamiento(data.get(id).get(3).trim());
		paginaxmPageObject.CantidadInversion(data.get(id).get(4).trim());
	}
	
	@Step
	public void guardarDatos() {
		// TODO Auto-generated method stub
		paginaxmPageObject.guarDatos();
	}



	
}
