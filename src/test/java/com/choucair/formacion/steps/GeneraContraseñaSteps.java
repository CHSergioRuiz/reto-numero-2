package com.choucair.formacion.steps;

import java.util.Random;

import com.choucair.formacion.pageobjects.GeneraContraseñaPageObjects;

import net.thucydides.core.annotations.Step;

public class GeneraContraseñaSteps 
{
	
	GeneraContraseñaPageObjects generaContraseñaPageObjects;
	
	@Step
	public void Contraseña() {
		// TODO Auto-generated method stub
		char [] chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
		int charsLength = chars.length;
		Random random = new Random();
		StringBuffer datoPrueba = new StringBuffer();

		for (int i=0;i<10;i++)
		{
			datoPrueba.append(chars[random.nextInt(charsLength)]);
		}
		
		System.out.println("Contraseña de la cuenta: " + datoPrueba.toString());
		generaContraseñaPageObjects.nuevaContraseña(datoPrueba.toString());
	}
	
	@Step
	public void ConfirmaContraseña() {
		char [] chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
		int charsLength = chars.length;
		Random random = new Random();
		StringBuffer datoPrueba = new StringBuffer();

		for (int i=0;i<10;i++)
		{
			datoPrueba.append(chars[random.nextInt(charsLength)]);
		}
		
		System.out.println("Confirmación de contraseña: " + datoPrueba.toString());
		generaContraseñaPageObjects.confirmaContraseña(datoPrueba.toString());
	}

}
