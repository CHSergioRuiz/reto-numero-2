package com.choucair.formacion.pageobjects;

import org.openqa.selenium.JavascriptExecutor;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl ("https://www.xm.com/register/account/demo?lang=es")
public class PaginaxmPageObject extends PageObject
{			
	@FindBy(xpath="//*[@id=\'cookieModal\']/div/div/div[1]/div[2]/div[2]/div/button")
	public WebElementFacade btnCookie;
	      
	@FindBy(xpath="//INPUT[@id='first_name']")
	public WebElementFacade txtPrimerNombre;

	@FindBy(xpath="//INPUT[@id='last_name']")
	public WebElementFacade txtApellido;

	@FindBy(xpath="//*[@id=\'country\']")
	public WebElementFacade cmbPaisDeResidencia;
	
	@FindBy(xpath="//INPUT[@id='city']")
	public WebElementFacade txtCiudad;
	
	@FindBy(xpath="//INPUT[@id='phone_number']")
	public WebElementFacade txtTelefono;

	@FindBy(xpath="//INPUT[@id='email']")
	public WebElementFacade txtEmail;

	@FindBy(xpath="//SELECT[@id='preferred_language']")
	public WebElementFacade cmbIdiomaPreferido;
	
	@FindBy(xpath="//SELECT[@id='trading_platform_type']")
	public WebElementFacade cmbTipoDePlataformaTrading;
	
	@FindBy(xpath="//SELECT[@id='account_type']")
	public WebElementFacade cmbTipoCuenta;
	
	@FindBy(xpath="//SELECT[@id='account_currency']")
	public WebElementFacade cmbMonedaBase;
	
	@FindBy(xpath="//SELECT[@id='account_leverage']")
	public WebElementFacade cmbApalancamiento;
	
	@FindBy(xpath="//SELECT[@id='investment_amount']")
	public WebElementFacade cmbCantidadInversion;
	
	@FindBy(xpath="//INPUT[@id='account_password']")
	public WebElementFacade txtContraseña;
	
	@FindBy(xpath="//INPUT[@id='account_password_confirmation']")
	public WebElementFacade txtConfirmaContraseña;
	
	@FindBy(xpath="//BUTTON[@id='submit-btn']")
	public WebElementFacade btnAgregarRegistro;
	
	public void btnCookie() {
		btnCookie.click();
	}
	
	public void PrimerNombre(String datoPrueba) {
		// TODO Auto-generated method stub
		txtPrimerNombre.click();
		txtPrimerNombre.clear();
		txtPrimerNombre.sendKeys(datoPrueba);
	}

	public void Apellido(String datoPrueba) {
		// TODO Auto-generated method stub
		txtApellido.click();
		txtApellido.clear();
		txtApellido.sendKeys(datoPrueba);
	}
	
	public void PaisDeResidencia(String datoPrueba) {
		// TODO Auto-generated method stub
		JavascriptExecutor jse = (JavascriptExecutor)getDriver();
		jse.executeScript("scroll(0, 250);");
		cmbPaisDeResidencia.click();
		cmbPaisDeResidencia.selectByVisibleText(datoPrueba);
	}

	public void Ciudad(String datoPrueba) {
		// TODO Auto-generated method stub
		txtCiudad.click();
		txtCiudad.clear();
		txtCiudad.sendKeys(datoPrueba);
	}

	public void Telefono(String datoPrueba) {
		// TODO Auto-generated method stub
		txtTelefono.click();
		txtTelefono.clear();
		txtTelefono.sendKeys(datoPrueba);
	}

	public void Email(String datoPrueba) {
		// TODO Auto-generated method stub
		txtEmail.click();
		txtEmail.clear();
		txtEmail.sendKeys(datoPrueba);
	}

	public void IdiomaPreferido(String datoPrueba) {
		// TODO Auto-generated method stub
		JavascriptExecutor jse = (JavascriptExecutor)getDriver();
		jse.executeScript("scroll(0, 450);");
		cmbIdiomaPreferido.click();
		cmbIdiomaPreferido.selectByVisibleText(datoPrueba);
	}

	public void TipoDePlataformaTrading(String datoPrueba) {
		// TODO Auto-generated method stub
		JavascriptExecutor jse = (JavascriptExecutor)getDriver();
		jse.executeScript("scroll(0, 450);");
		cmbTipoDePlataformaTrading.click();
		cmbTipoDePlataformaTrading.selectByVisibleText(datoPrueba);
	}

	public void TipoCuenta(String datoPrueba) {
		// TODO Auto-generated method stub
		JavascriptExecutor jse = (JavascriptExecutor)getDriver();
		jse.executeScript("scroll(0, 450);");
		cmbTipoCuenta.click();
		cmbTipoCuenta.selectByVisibleText(datoPrueba);
	}

	public void MonedaBase(String datoPrueba) {
		// TODO Auto-generated method stub
		JavascriptExecutor jse = (JavascriptExecutor)getDriver();
		jse.executeScript("scroll(0, 550);");
		cmbMonedaBase.click();
		cmbMonedaBase.selectByVisibleText(datoPrueba);
	}

	public void Apalancamiento(String datoPrueba) {
		// TODO Auto-generated method stub
		JavascriptExecutor jse = (JavascriptExecutor)getDriver();
		jse.executeScript("scroll(0, 550);");
		cmbApalancamiento.click();
		cmbApalancamiento.selectByVisibleText(datoPrueba);
	}

	public void CantidadInversion(String datoPrueba) {
		// TODO Auto-generated method stub
		JavascriptExecutor jse = (JavascriptExecutor)getDriver();
		jse.executeScript("scroll(0, 650);");
		cmbCantidadInversion.click();
		cmbCantidadInversion.selectByVisibleText(datoPrueba);
	}

	public void Contraseña(String datoPrueba) {
		// TODO Auto-generated method stub
		JavascriptExecutor jse = (JavascriptExecutor)getDriver();
		jse.executeScript("scroll(0, 900);");
		txtContraseña.click();
		txtContraseña.clear();
		txtContraseña.sendKeys(datoPrueba);
	}

	public void ConfirmaContraseña(String datoPrueba) {
		// TODO Auto-generated method stub
		JavascriptExecutor jse = (JavascriptExecutor)getDriver();
		jse.executeScript("scroll(0, 900);");
		txtConfirmaContraseña.click();
		txtConfirmaContraseña.clear();
		txtConfirmaContraseña.sendKeys(datoPrueba);
	}
	
	public void guarDatos() {
		// TODO Auto-generated method stub
		JavascriptExecutor jse = (JavascriptExecutor)getDriver();
		jse.executeScript("scroll(0, 1200);");
		btnAgregarRegistro.click();	 
	}
		
}
