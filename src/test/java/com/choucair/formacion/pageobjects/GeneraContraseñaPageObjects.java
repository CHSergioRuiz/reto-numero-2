package com.choucair.formacion.pageobjects;

import org.openqa.selenium.JavascriptExecutor;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;

public class GeneraContraseñaPageObjects extends PageObject
{

	String contraseñaCuenta = "//INPUT[@id='account_password']";
	String contraseñaCuentaConf = "//INPUT[@id='account_password_confirmation']";
	
	public void posicionarElementoString(String Element) {
        ((JavascriptExecutor) getDriver()).executeScript(
                "arguments[0].scrollIntoView(true); arguments[0].style.border='1px dashed red';",
                getDriver().findElements(By.xpath(Element)).get(0));
    }
	
	public void nuevaContraseña(String buffer) 
	{
		// TODO Auto-generated method stub
		posicionarElementoString(contraseñaCuenta);
		findBy(contraseñaCuenta).type(buffer);
	}

	public void confirmaContraseña(String bufferDos) 
	{
		// TODO Auto-generated method stub
		posicionarElementoString(contraseñaCuentaConf);
		findBy(contraseñaCuentaConf).type(bufferDos);
	}
	
}
