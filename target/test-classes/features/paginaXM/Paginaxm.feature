#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Prueba Registro en pagina XM

  @CasoExitoso
  Scenario: Registrar datos en la pagina XM
    Given Ingresar a la pagina y aceptar Cookies
    When Ingresar Datos Personales
    | PrimerNombre 	| Apellido 	| PaisDeResidencia	|	Ciudad	| Telefono  | Email	         					|	IdiomaPreferido		|
		| Muhammad 			| Saleem  	|	Afghanistan				|	Kabul		| 3262920		| MuhammadS@hottmail.com 	|	Inglés						|
    And Agregar datos de cuenta de trading
    | TipoDePlataformaTrading																				| TipoCuenta														| MonedaBase	| Apalancamiento	| CantidadInversion	 	|
    |	MT4 (forex, CFDs sobre índices bursátiles, metales, energías)	|	XM Ultra Low Standard (1 lote=100,000)|	AUD					|	1:888						|	5.000.000						|
    And Crear Contraseña 
    | Contraseña		| ConfirmaContraseña 	|
    |	Colombia2019	|	Colombia2019				|
    Then Abrir Cuenta demo

  @CasoNombreCorrigeErr
  Scenario: Registrar datos en la pagina XM con error en el nombre
    Given Ingresar a la pagina y aceptar Cookies
    When Ingresar Datos Personales
    | PrimerNombre 	| Apellido 	| PaisDeResidencia	|	Ciudad	| Telefono  | Email	         					|	IdiomaPreferido		|
		| Andrés	 			| Ramirez  	|	Venezuela					|	Caracas	| 3262921		| aramirez@hottmail.com 	|	Bengalí						|
		| Andres	 			| Ramirez  	|	Venezuela					|	Caracas	| 3262921		| aramirez@hottmail.com 	|	Bengalí						|
    And Agregar datos de cuenta de trading
    | TipoDePlataformaTrading																										| TipoCuenta														| MonedaBase	| Apalancamiento	| CantidadInversion	 	|
    |	MT5 (forex, CFDs sobre acciones, índices bursátiles, metales y energías)	|	XM Ultra Low Standard (1 lote=100,000)|	SGD					|	1:66						|	5.000								|
    And Crear Contraseña 
    | Contraseña		| ConfirmaContraseña 	|
    |	Colombia2019	|	Colombia2019				|
    Then Abrir Cuenta demo
    
    @CasoValidaContraseña
  Scenario: Verificar campo contraseña
     Given Ingresar a la pagina y aceptar Cookies
    When Ingresar Datos Personales
    | PrimerNombre 	| Apellido 	| PaisDeResidencia	|	Ciudad					| Telefono  | Email	         					|	IdiomaPreferido		|
		| Carlos	 			| Ramos	  	|	Burkina Faso			|	Bobo-Dioulasso	| 3262922		| cramos@hottmail.com		 	|	Alemán						|
    And Agregar datos de cuenta de trading
    | TipoDePlataformaTrading																										| TipoCuenta														| MonedaBase	| Apalancamiento	| CantidadInversion	 	|
    |	MT5 (forex, CFDs sobre acciones, índices bursátiles, metales y energías)	|	XM Ultra Low Standard (1 lote=100,000)|	SGD					|	1:5							|	100.000							|
    And Crear Contraseña 
    | Contraseña		| ConfirmaContraseña 	|
    |	Colombia2019	|	Colombia2017				|
		Then Abrir Cuenta demo
		And Crear Contraseña
		| Contraseña		| ConfirmaContraseña 	|
		|	Colombia2019	|											|
    |	Colombia2019	|	Colombia2019				|
    Then Abrir Cuenta demo
    
    @CasoRandomError
  Scenario: Creacion contraseñas diferentes
    Given Ingresar a la pagina y aceptar Cookies
    When Ingresar Datos Personales
    | PrimerNombre 	| Apellido 	| PaisDeResidencia	|	Ciudad			| Telefono  | Email	         					|	IdiomaPreferido		|
		| Anderson 			| Bojaca  	|	Colombia					|	Medellin		| 3262923		| abojaca@hottmail.com	 	|	Inglés						|
    And Agregar datos de cuenta de trading
    | TipoDePlataformaTrading																				| TipoCuenta														| MonedaBase	| Apalancamiento	| CantidadInversion	 	|
    |	MT4 (forex, CFDs sobre índices bursátiles, metales, energías)	|	XM Ultra Low Standard (1 lote=100,000)|	AUD					|	1:888						|	5.000								|
    Then Ingresar contraseña
    And Abrir Cuenta demo


#    Examples: 
#      | name  | value | status  |
#      | name1 |     5 | success |
#      | name2 |     7 | Fail    |
